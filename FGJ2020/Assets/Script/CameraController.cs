﻿using CliffLeeCL;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Transform PlayerTransform;
    public GameObject spawnPoint;
    public GameObject meteorPrefab;
    private float SpawnTime;
    private static bool IsStop = false;

    private float MoveSpeed = 0f;
    public float MoveSpeedLow = 15;
    public float MoveSpeedHigh = 20;

    public Vector3 Offset;

    private void Start()
    {
    }
    private void OnEnable() {
        IsStop = false;
    }
    public static void Stop()
    {
        IsStop = true;
    }

    private void Update()
    {
        SpawnTime += Time.deltaTime;
        if (SpawnTime >= 7f)
        {
            SpawnTime -= Random.Range(5,7);
            Instantiate(meteorPrefab, new Vector3(spawnPoint.transform.position.x, spawnPoint.transform.position.y, 0), Quaternion.identity, spawnPoint.transform);
        }
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        UpdateSpeed();
        CameraMove();
    }

    private void UpdateSpeed()
    {
        if (PlayerController.instance.transform != null)
        {
            if (transform.position.x > PlayerController.instance.transform.position.x)
            {
                MoveSpeed = MoveSpeedLow;
            }
            else
            {
                MoveSpeed = MoveSpeedHigh;
            }
        }
    }

    private void CameraMove()
    {
        if(IsStop) return;
        //float PosY = Mathf.Lerp(transform.position.y, PlayerTransform.position.y, 0.3f);
        //transform.position = new Vector3(transform.position.x, PosY, transform.position.z) + Offset;
        transform.position += new Vector3(MoveSpeed * Time.deltaTime, 0, 0);
    }
}