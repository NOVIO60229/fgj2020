﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;
using MoreMountains.Tools;

public class AudioController : PersistentSingleton<AudioController> {

    public AudioSource MusicAudio;
    public AudioSource MusicAudio2;
    public Music[] MusicPool;
    bool InputLock = false; //prevent too many input in short time
    Music CurrentMusic;
    Music NextMusic;
    bool VolumeDownToZero = false;

    [Space(30)]
    public AudioSource SEAudio;
    public SE[] SoundEffectPool;

    public void PlayBGM(string name)
    {
        if (InputLock == false)
        {
            InputLock = true;

            CurrentMusic = CurrentPlaying();
            NextMusic = NextPlay(name);
            if (CurrentMusic == null)
            {
                MusicToAudioSource(NextMusic);
                MusicAudio.Play();
                InputLock = false;
            }
            else
            {
                StartCoroutine(DecreaseVolume()); // change music 
            }
        }
    }

    public Music CurrentPlaying()
    {
        if (MusicAudio.isPlaying)
        {
            foreach (Music s in MusicPool)
            {
                if (s.clip == MusicAudio.clip)
                {
                    return s;
                }
            }
        }
        else if (MusicAudio2.isPlaying)
        {
            foreach (Music s in MusicPool)
            {
                if (s.clip == MusicAudio.clip)
                {
                    return s;
                }
            }
        }
        else return null;

        return null;
    }
    public Music NextPlay(string name)
    {
        Music s = Array.Find(MusicPool, sound => sound.name == name);
        if (s == null)
        {
            return null;
        }
        else return s;
    }
    public void MusicToAudioSource(Music s)
    {
        if (!MusicAudio.isPlaying)
        {
            MusicAudio.clip = s.clip;
            MusicAudio.volume = s.volume;
        }
        else if (!MusicAudio2.isPlaying)
        {
            MusicAudio2.clip = s.clip;
            MusicAudio2.volume = s.volume;
        }
    }
    IEnumerator DecreaseVolume()
    {
        while (true)
        {
            if (MusicAudio.isPlaying)
            {
                if (MusicAudio.volume > 0.04f)
                {
                    MusicAudio.volume -= 0.04f;
                }
                else
                {
                    StartCoroutine(IncreaseVolume());
                    break;
                }
            }
            else if (MusicAudio2.isPlaying)
            {
                if (MusicAudio2.volume > 0.04f)
                {
                    MusicAudio2.volume -= 0.04f;
                }
                else
                {
                    StartCoroutine(IncreaseVolume());
                    break;
                }
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
    IEnumerator IncreaseVolume()
    {
        int changingphase = 0;
        int audionum = 0;          //which audio was using
        float tempVol = NextMusic.volume;
        while (changingphase == 0) // play next music
        {
            MusicToAudioSource(NextMusic);
            if (MusicAudio.isPlaying)
            {
                MusicAudio2.volume = 0f;
                MusicAudio2.Play();
                changingphase = 1;
                audionum = 1;
            }
            else if (MusicAudio2.isPlaying)
            {
                MusicAudio.volume = 0f;
                MusicAudio.Play();
                changingphase = 1;
                audionum = 2;
            }
            yield return new WaitForSeconds(0.1f);
        }
        while (changingphase == 1) // increase next music vol and stop previous audio
        {
            if (audionum == 1)
            {
                MusicAudio.volume -= 0.05f;
                MusicAudio2.volume += 0.05f;
                if (MusicAudio.volume<=0f && MusicAudio2.volume>= tempVol)
                {
                    MusicAudio.Stop();
                    InputLock = false;
                    break;
                }
            }
            else if (audionum == 2)
            {
                MusicAudio2.volume -= 0.05f;
                MusicAudio.volume += 0.05f;
                if (MusicAudio2.volume <= 0f && MusicAudio.volume >= tempVol)
                {
                    MusicAudio2.Stop();
                    InputLock = false;
                    break;
                }
            }
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void PlaySE(string name)
    {
        SE se = Array.Find(SoundEffectPool,sound => sound.name == name);
        SEAudio.clip = se.clip;
        SEAudio.volume = se.volume;
        SEAudio.Play();
    }
}
