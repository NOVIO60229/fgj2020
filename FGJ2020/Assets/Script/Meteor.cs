﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Meteor : MonoBehaviour
{
    public GameObject fire;
    private Rigidbody2D Rb;

    private Vector2 preDir = new Vector2(-1f, -0.1f);
    private Vector2 lateDir = new Vector2(-1f, -1);
    private Vector2 Dir;
    private float speed = 1;

    private void Start()
    {
        Rb = GetComponent<Rigidbody2D>();

        StartCoroutine(Flying());
    }

    private void Update()
    {
        if (speed != 0)
        {
            Rb.velocity = Dir * speed;
        }
    }

    private IEnumerator Flying()
    {
        DOTween.To(() => speed, x => speed = x, 0.4f, 2).From(0.1f, true);
        Dir = preDir;
        transform.DOScale(0.3f, 2f);

        yield return new WaitForSeconds(2f);

        DOTween.To(() => speed, x => speed = x, 10, 1).From(1f, true);
        Dir = lateDir;
        transform.SetParent(null);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (fire != null)
        {
            fire.GetComponent<SpriteRenderer>().DOFade(0, 0.4f).onComplete += () => Destroy(fire);
        }
        speed = 0;
    }
}