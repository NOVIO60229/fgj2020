using UnityEngine;
using Sirenix.OdinInspector;

namespace MoreMountains.Tools
{
	/// <summary>
	/// Singleton pattern.
	/// </summary>
	public class Singleton<T> : SerializedMonoBehaviour where T : Component
	{
		protected static T _instance;
        static bool _isDestroyed;

		/// <summary>
		/// Singleton design pattern
		/// </summary>
		/// <value>The instance.</value>
		public static T Instance
		{
			get
			{
                if (_isDestroyed)
                    return null;

				if (_instance == null)
				{
					_instance = FindObjectOfType<T> ();
					if (_instance == null)
					{
						GameObject obj = new GameObject ();
						_instance = obj.AddComponent<T> ();
                        obj.transform.name = typeof(T).ToString();
					}
				}
				return _instance;
			}
		}

	    /// <summary>
	    /// On awake, we initialize our instance. Make sure to call base.Awake() in override if you need awake.
	    /// </summary>
	    protected virtual void Awake ()
		{
			_instance = this as T;
            Application.quitting += Quitting;
		}

        void Quitting()
        {
            Application.quitting -= Quitting;
            _isDestroyed = true;
        }
	}
}
