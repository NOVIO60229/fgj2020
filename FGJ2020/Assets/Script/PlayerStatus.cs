﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public enum State
{
    normal,
    slide,
    crouch,
    hurt
}

public class PlayerStatus : MonoBehaviour
{
    //run variables
    public const float BaseRunSpeed = 12f;

    private List<SpeedEffect> SpeedEffectList = new List<SpeedEffect>();

    //Damage variable
    public int HP { get; set; } = 100;

    //jump variable
    public LayerMask GroundLayers;

    //slide variables
    public const float BaseSlideSpeed = 5f;

    //crouch variables
    public const float BaseCrouchSpeed = 6f;

    public float CrouchSpeedMultiplier = 0.4f;

    public void AddSpeedEffect(float speed, float sec, Action startAction = null, Action endAction = null)
    {
        SpeedEffect effect = new SpeedEffect(speed, sec, startAction, endAction);
        SpeedEffectList.Add(effect);
    }

    public float GetRunSpeed()
    {
        return BaseRunSpeed + GetSpeedEffectSum();
    }

    public float GetSlideSpeed()
    {
        return BaseSlideSpeed + GetSpeedEffectSum();
    }

    public float GetCrouchSpeed()
    {
        return BaseCrouchSpeed + GetSpeedEffectSum();
    }

    private float GetSpeedEffectSum()
    {
        float SpeedChange = 0;
        foreach (SpeedEffect effect in SpeedEffectList)
        {
            SpeedChange += effect.GetSpeed();
        }
        return SpeedChange;
    }

    private void Update()
    {
        UpdateSpeedEffectList();
    }

    private void UpdateSpeedEffectList()
    {
        for (int i = SpeedEffectList.Count - 1; i >= 0; i--)
        {
            if (SpeedEffectList[i].IsEffectEnd())
            {
                SpeedEffectList[i].OnRemove();
                SpeedEffectList.RemoveAt(i);
            }
        }
    }
}

internal class SpeedEffect
{
    private float Speed;
    private float DurationSec;
    private float StartTime;
    private Action StartAction;
    private Action EndAction;

    public SpeedEffect(float speed, float durationSec, Action startAction, Action endAction)
    {
        DurationSec = durationSec;
        StartTime = Time.time;
        DOTween.To(() => Speed, x => Speed = x, 0, durationSec).From(speed, true).SetEase(Ease.OutQuart);

        OnStart();
    }

    public bool IsEffectEnd()
    {
        return Time.time - StartTime > DurationSec;
    }

    public float GetSpeed()
    {
        return Speed;
    }

    public void OnStart()
    {
        StartAction?.Invoke();
    }

    public void OnRemove()
    {
        EndAction?.Invoke();
    }
}