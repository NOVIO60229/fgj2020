﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using UnityEngine.InputSystem;
using System.Threading;
using DG.Tweening.Core.Easing;
using CliffLeeCL;

public class PlayerController : MonoBehaviour
{
    [HideInInspector] public State state = State.normal;

    public static PlayerController instance;

    //components
    [HideInInspector] public Rigidbody2D Rb;

    private SpriteRenderer SpriRen;
    private BoxCollider2D BoxCollider;
    private PlayerAnimation PlayerAnimation;
    private PlayerStatus Status;

    //player state
    [HideInInspector] public bool IsGrounded = false;

    [HideInInspector] public bool IsHurt = false;
    [HideInInspector] public bool CanNotMove = false;
    private bool IsInvincible = false;
    [HideInInspector] public Vector2 FacingDir = Vector2.right;

    //run variables
    [HideInInspector] public Vector2 InputDir = Vector2.zero;

    public LayerMask ScreenWallLayer;

    //Damage variable
    public int HP { get; set; } = 100;

    private float KnockBackForce = 40f;
    private bool IsKnocked = false;
    private Vector2 KnockDir = Vector2.zero;
    private float HurtTime = 0.7f;

    //jump variable
    private bool IsJumpButtonDown = false;

    private bool IsJumpButtomPressed = false;
    private float JumpForce = 40f;
    public LayerMask GroundLayers;
    private float FallMultiplier = 4.5f;
    private float LowJumpMultiplier = 15f;
    private float LastJumpMultiplier = 7f;
    private int JumpTimes = 1;
    private int JumpTimeLimit = 1;
    public bool IsBouncing { get; set; } = false;
    private bool IsJustStartBounce = false;
    private float IniScaleY;
    public float Bounciness = 1f;
    public float BounceRate = 30;
    public float BounceProtectSec = 0.01f;

    //slide variables
    private float SlideTime = 0.45f;

    private float SlideCurrentTime = 0f;
    private float SlideInvincibleTimePercent = 0.1f;
    private bool IsSlideCD = false;
    private float SlideCDTime = 0.2f;
    private float SlideSpeedBoost = 35f;
    private float SlideSpeedBoostSec = 1.5f;
    private bool IsAniFromSlideToIdle = false;
    private bool IsAbleCancelSlide = false;

    //crouch variables
    private bool IsCrouchButtonDown = false;

    private bool IsCrouchButtonPressed = false;

    private void Awake()
    {
        instance = this;
    }

    private void OnEnable()
    {
    }

    private void OnDisable()
    {
    }

    private void Start()
    {
        Rb = GetComponent<Rigidbody2D>();
        SpriRen = GetComponent<SpriteRenderer>();
        BoxCollider = GetComponent<BoxCollider2D>();
        PlayerAnimation = GetComponent<PlayerAnimation>();
        Status = GetComponent<PlayerStatus>();

        IniScaleY = transform.localScale.y;
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        InputDir = context.ReadValue<Vector2>();
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            IsJumpButtonDown = true;
            IsJumpButtomPressed = true;
        }
        if (context.canceled)
        {
            IsJumpButtomPressed = false;
        }
    }

    public void OnCrouch(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            IsCrouchButtonDown = true;
            IsCrouchButtonPressed = true;
            Crouch();
        }
        if (context.canceled)
        {
            IsCrouchButtonPressed = false;
        }
    }

    private void Crouch()
    {
        if (!IsGrounded) return;

        if (IsCrouchButtonPressed && InputDir.x == 0)
        {
            state = State.crouch;
            SetColliderSlideMode();
        }
        else if (!IsSlideCD && state == State.normal)
        {
            Slide();
        }
    }

    private void Slide()
    {
        IsSlideCD = true;
        state = State.slide;
        SetColliderSlideMode();
        SlideCurrentTime = 0;
        Status.AddSpeedEffect(SlideSpeedBoost, SlideSpeedBoostSec);
        PlayerAnimation.PlayAnimationOnce("Slide");

        IsAbleCancelSlide = false;
        Timer(PlayerAnimation._aniSecDic["Slide"], () => IsAbleCancelSlide = true);
    }

    private void Update()
    {
        CheckGrounded();
        switch (state)
        {
            case State.normal:

                UpdateAnimation();
                AdjustGravityForJump();
                CheckFlip();
                break;

            case State.slide:
                SlideUpdate();
                break;

            case State.crouch:
                CrouchUpdate();
                break;

            default:
                Debug.LogError("Not in any state");
                break;
        }
    }

    private void CheckGrounded()
    {
        Collider2D hit = Physics2D.OverlapBox(
            (Vector2)BoxCollider.bounds.center - new Vector2(0, BoxCollider.bounds.extents.y + 0.1f), new Vector2(BoxCollider.bounds.size.x * 0.9f, 0.01f), 0, GroundLayers);
        IsGrounded = hit ? true : false;

        if (IsGrounded)
        {
            JumpTimes = JumpTimeLimit;
        }
    }

    private void AdjustGravityForJump()
    {
        if (!IsGrounded)
        {
            if (Rb.velocity.y < 0)
            {
                Rb.velocity += Vector2.up * Physics2D.gravity.y * FallMultiplier * Time.deltaTime;
            }
            //add extra force on the last jump if holding button
            else if (Rb.velocity.y > 0 && JumpTimes == 0 && IsJumpButtomPressed)
            {
                Rb.velocity += Vector2.up * Physics2D.gravity.y * LastJumpMultiplier * Time.deltaTime;
            }
            else if (Rb.velocity.y > 0)
            {
                Rb.velocity += Vector2.up * Physics2D.gravity.y * LowJumpMultiplier * Time.deltaTime;
            }
        }
    }

    private void CheckFlip()
    {
        if (InputDir.x < 0)
        {
            FacingDir = Vector2.left;
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        }
        else if (InputDir.x > 0)
        {
            FacingDir = Vector2.right;
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
    }

    private void SlideUpdate()
    {
        CheckInvincible();
        CheckInteruptSlide();
    }

    private void CheckInteruptSlide()
    {
        if (Status.GetSlideSpeed() <= PlayerStatus.BaseCrouchSpeed)
        {
            state = State.crouch;
            Timer(SlideCDTime, () => IsSlideCD = false);
        }
        //else if (IsToNormalColliderStuck())
        //{
        //    return;
        //}
        else if (!IsCrouchButtonPressed && IsAbleCancelSlide)
        {
            state = State.normal;
            SetColliderNormalMode();
            IsAniFromSlideToIdle = true;
            Timer(PlayerAnimation._aniSecDic["SlideToIdle"], () => IsAniFromSlideToIdle = false);
            Timer(SlideCDTime, () => IsSlideCD = false);

            Bounce();
        }
        else if (IsJumpButtonDown)
        {
            state = State.normal;
            SetColliderNormalMode();
            Timer(SlideCDTime, () => IsSlideCD = false);
        }
    }

    private void SetColliderSlideMode()
    {
        transform.localScale = new Vector3(transform.localScale.x, IniScaleY * 0.5f, transform.localScale.z);
        Rb.AddForce(Vector2.down * 50f);
    }

    private void SetColliderNormalMode()
    {
        transform.localScale = new Vector3(transform.localScale.x, IniScaleY, transform.localScale.z);
    }

    private bool IsToNormalColliderStuck()
    {
        SetColliderNormalMode();
        Collider2D hit = Physics2D.OverlapBox(BoxCollider.bounds.center, new Vector2(BoxCollider.bounds.size.x * 0.9f, BoxCollider.bounds.size.y * 0.9f), 0, GroundLayers);
        bool IsStuck = hit ? true : false;
        SetColliderSlideMode();
        return IsStuck;
    }

    private void CheckInvincible()
    {
        SlideCurrentTime += Time.deltaTime;
        if (SlideCurrentTime < SlideTime * SlideInvincibleTimePercent)
        {
            IsInvincible = true;
        }
        else
        {
            IsInvincible = false;
        }
    }

    private void CrouchUpdate()
    {
        CheckInteruptCrouch();
        PlayCrouchAnimation();
        CheckFlip();
    }

    private void CheckInteruptCrouch()
    {
        //if (IsToNormalColliderStuck()) return;

        if (IsJumpButtonDown || !IsCrouchButtonPressed)
        {
            state = State.normal;
            SetColliderNormalMode();
            Bounce();
        }
    }

    private void PlayCrouchAnimation()
    {
        if (InputDir.x != 0)
        {
            PlayerAnimation.PlayAnimation("Crouch");
        }
        else
        {
            PlayerAnimation.PlayAnimation("CrouchIdle");
        }
    }

    private void LateUpdate()
    {
        ClearInput();
    }

    private void ClearInput()
    {
        IsCrouchButtonDown = false;
    }

    private void FixedUpdate()
    {
        if (IsKnocked)
        {
            Rb.velocity = KnockDir * KnockBackForce;
            IsKnocked = false;
        }

        switch (state)
        {
            case State.normal:

                Rb.velocity = new Vector2(InputDir.x * Status.GetRunSpeed(), Rb.velocity.y);
                break;

            case State.slide:

                Rb.velocity = new Vector2(FacingDir.x * Status.GetSlideSpeed(), Rb.velocity.y);
                break;

            case State.crouch:

                Rb.velocity = new Vector2(InputDir.x * Status.GetCrouchSpeed(), Rb.velocity.y);
                break;

            default:

                break;
        }

        if (IsJumpButtonDown)
        {
            if (state == State.slide && IsToNormalColliderStuck())
            {
                return;
            }

            Jump();
        }
    }

    private void UpdateAnimation()
    {
        if (Rb.velocity.y > 0.1f)
        {
            PlayerAnimation.PlayAnimation("JumpUp");
        }
        else if (Rb.velocity.y < -0.1f)
        {
            PlayerAnimation.PlayAnimation("JumpDown");
        }
        else if (IsAniFromSlideToIdle)
        {
            PlayerAnimation.PlayAnimation("SlideToIdle");
        }
        else if (InputDir.x != 0)
        {
            PlayerAnimation.Play_Run(Status.GetRunSpeed());
        }
        else
        {
            PlayerAnimation.PlayAnimation("Idle");
        }
    }

    private void Jump()
    {
        IsJumpButtonDown = false;
        if (JumpTimes == 0) return;

        Rb.velocity = new Vector2(0, JumpForce);
        JumpTimes -= 1;
        AudioManager.Instance.PlaySoundRandomClipAndPitch(AudioManager.AudioName.Jump);
    }

    private void Bounce()
    {
        StartCoroutine(DoBounce());
    }

    private IEnumerator DoBounce()
    {
        IsJustStartBounce = true;
        Timer(BounceProtectSec, () => IsJustStartBounce = false);
        IsBouncing = true;
        Jump();
        float time = 0;
        float scaleMulti = 1f;
        DOTween.To(() => scaleMulti, x => scaleMulti = x, 0.6f, 0.4f);
        while (IsBouncing)
        {
            float scaleAdd = (Mathf.Sin(time * BounceRate) * 0.2f + 1) * Bounciness;
            transform.localScale = new Vector3(transform.localScale.x, IniScaleY * scaleAdd * scaleMulti, transform.localScale.z);
            time += Time.deltaTime;

            if (Rb.velocity.y < 0)
            {
                IsBouncing = false;
            }

            yield return null;
        }
        transform.localScale = new Vector3(transform.localScale.x, IniScaleY, transform.localScale.z);
    }

    public void Hurt(int damage, Transform attacker)
    {
        if (IsHurt || IsInvincible) return;

        Status.AddSpeedEffect(-15, 0.3f);

        //vibrate
        if (Gamepad.current != null)
        {
            Gamepad.current.SetMotorSpeeds(0.5f, 0.5f);
            Timer(0.2f, () => Gamepad.current.SetMotorSpeeds(0, 0f));
        }

        HP -= damage;

        IsKnocked = true;
        //knock direction depends on position of attacker
        KnockDir = attacker.position.x > transform.position.x ? new Vector2(-1, 0.2f) : new Vector2(1, 0.2f);

        StartCoroutine(HurtReaction());
    }

    public IEnumerator HurtReaction()
    {
        IsHurt = true;
        yield return Blinking(HurtTime);
        IsHurt = false;
    }

    public IEnumerator Blinking(float duration)
    {
        float timeLeft = duration;

        //shine red light effect variables
        float switchColorTime = 0.08f;
        float colorTime = switchColorTime;
        bool isColorRed = true;

        while (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;

            //shine red light effect
            colorTime -= Time.deltaTime;
            if (colorTime <= 0)
            {
                colorTime += switchColorTime;
                isColorRed = !isColorRed;

                if (isColorRed)
                    SpriRen.color = Color.red;
                else
                    SpriRen.color = Color.white;
            }
            yield return null;
        }
        SpriRen.color = Color.white;
    }

    public void Timer(float time, Action EndOperation)
    {
        StartCoroutine(TimerCoroutine(time, EndOperation));
    }

    public IEnumerator TimerCoroutine(float time, Action EndOperation)
    {
        yield return new WaitForSeconds(time);
        EndOperation?.Invoke();
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Explodable"))
        {
            Explodable ex = collision.transform.GetComponent<Explodable>();
            if (ex != null)
            {
                ex.Explode(collision.GetContact(0).point);
            }
        }
        else if (collision.transform.CompareTag("Platform") && IsBouncing && !IsJustStartBounce)
        {
            Explodable ex = collision.transform.GetComponent<Explodable>();
            if (ex != null)
            {
                ex.Explode(collision.GetContact(0).point);
            }
        }
    }
}