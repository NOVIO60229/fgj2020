﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewsDialogue
{
    public int ID;
    [TextArea]
    public string Content;
}
