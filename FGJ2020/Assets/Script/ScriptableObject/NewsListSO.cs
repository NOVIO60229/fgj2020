﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
[CreateAssetMenu(menuName = "ScriptableObject/NewsList")]
public class NewsListSO : SerializedScriptableObject
{
    public List<NewsDialogue> NewsList = new List<NewsDialogue>();
}
