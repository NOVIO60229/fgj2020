﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using MoreMountains.Tools;
using DG.Tweening;
using Sirenix.OdinInspector;

public class MainController : PersistentSingleton<MainController>
{
    //場景讀取
    public bool LoadingSceneLock { get; private set; }     // call SceneManager.LoadScene() 之前會 = true，讓其他人的OnDisable()可以濾掉一些事情，隨後會自動關上
    public GameObject LoadingScreen;
    public Color LoadingScreenColor;
    public ResultPanel Result;
    public Ingame Ingame;

    //private Dictionary<Obj, int> destroyedObjectList;

    protected override void Awake()
    {
        base.Awake();
        // 切Scene完成的時候跑一些callback
        SceneManager.sceneLoaded += SceneLoaded;
        // 切Scene時管制其他Singleton的存取行為 (sceneUnloaded比前一個場景的OnDestroy慢)
        SceneManager.sceneUnloaded += SceneUnloaded;
    }

    #region 場景讀取函式
    public void LoadScene(string sceneName)
    {
        LoadingSceneLock = true;
        //SceneManager.LoadScene(sceneName);
        LoadingScreen.SetActive(true);
        StartCoroutine(LoadAsyncOperation(sceneName));
    }

    IEnumerator LoadAsyncOperation(string sceneName)
    {
        while(LoadingScreen.transform.GetChild(0).GetComponent<Image>().color.a < 1)
        {
            LoadingScreenColor.a += 0.1f;
            LoadingScreen.transform.GetChild(0).GetComponent<Image>().color = LoadingScreenColor;
            yield return new WaitForSeconds(0.05f);
        }

        if(LoadingScreen.transform.GetChild(0).GetComponent<Image>().color.a >=1)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
            while(asyncLoad.progress < 1)
            {
                //進度條的動畫放這裡
                yield return new WaitForEndOfFrame();
            }       
        }

        while(LoadingScreen.transform.GetChild(0).GetComponent<Image>().color.a > 0)
        {
            LoadingScreenColor.a -= 0.1f;
            LoadingScreen.transform.GetChild(0).GetComponent<Image>().color = LoadingScreenColor;
            yield return new WaitForSeconds(0.025f);
        }

        LoadingScreen.SetActive(false);
    }

    void SceneLoaded(Scene scene, LoadSceneMode mode)
    {
        OnSceneLoadedCallback(scene);
    }

    // Unloaded在前一場景的OnDestroy之後，此時已可解除鎖定
    void SceneUnloaded(Scene scene)
    {
        LoadingSceneLock = false;
    }

    void OnSceneLoadedCallback(Scene scene)
    {
        if (scene.name == "Title")
        {

        }
        else if (scene.name == "Main")
        {
            ShowCurrentCost();
        }
    }
    #endregion 場景讀取函式

    [Button]
    public void ShowRersult(bool failed)
    {
        Result.OpenResultPanel(failed);
    }

    [Button]
    public void ShowCurrentCost()
    {
        Ingame.ShowCurrentCost();
    }

    private void OnDisable() 
    {
        SceneManager.sceneLoaded -= SceneLoaded;
        SceneManager.sceneUnloaded -= SceneUnloaded;
    }
}
