﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using CliffLeeCL;
using Sirenix.OdinInspector;

public class Ingame : MonoBehaviour
{
    public GameObject CurrentCostText;
    int totalCost = 0;

    void Start()
    {
        
    }

    void Update()
    {
        if(CurrentCostText.activeSelf)
        {
            totalCost = 0;
            foreach(var o in ScoreManager.Instance.obstacleToDestroyCount.Keys)
                totalCost +=  ScoreManager.Instance.obstacleToDestroyCount[o]* o.value;

            CurrentCostText.GetComponent<TextMeshProUGUI>().text = "賠償金額： " + totalCost;
        }
    }

    [Button]
    public void ShowCurrentCost()
    {
        CurrentCostText.SetActive(true);
        CurrentCostText.GetComponent<TextMeshProUGUI>().text = "賠償金額： " + "0";
    }

    [Button]
    public void CloseCurrentCost()
    {
        CurrentCostText.SetActive(false);
        CurrentCostText.GetComponent<TextMeshProUGUI>().text = "";
    }
}
