﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleUI : MonoBehaviour
{
    public Button StartButton;

    void Start()
    {
        StartButton.onClick.AddListener(()=>{
            MainController.Instance.LoadScene("Main");
        ;});
    }

    void Update()
    {
        
    }
}
