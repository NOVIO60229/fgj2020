﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using CliffLeeCL;
using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;

public class ResultPanel : MonoBehaviour
{
    public GameObject MainPanel;
    public GameObject ScrollContent, ScrollBar, ScorePanel, NewsPanel, NextButton;
    public List<Sprite> RatingIcon = new List<Sprite>();
    public List<Sprite> NewsPicture = new List<Sprite>();
    public NewsListSO NewsList, NewsSubList;

    [SerializeField]
    List<Obstacle> DestroyedList = new List<Obstacle>();
    int TotalCost = 0;
    bool isFailed = false;
    int RateS = 25000, RateA = 50000, RateB = 100000, RateC = 400000;


    //開啟並刷新選單
    void RefreshResultPanel()
    {
        ScrollBar.GetComponent<Scrollbar>().value = 1f;
        ClearResultPanel();

        int objectCount = DestroyedList.Count;

        //確保按鈕數量符合
        for (int i = ScrollContent.transform.childCount; i > objectCount; i--) 
            ScrollContent.transform.GetChild(i - 1).gameObject.SetActive(false);

        //生成物件
        for(int i= 0; i<objectCount; i++)
        {
            if (ScrollContent.transform.childCount < objectCount)
                Instantiate(ScrollContent.transform.GetChild(0), ScrollContent.transform);
            else
                ScrollContent.transform.GetChild(i).gameObject.SetActive(true);

            var child = ScrollContent.transform.GetChild(i);
            var icon = child.GetComponent<Image>();
            var countText = child.GetChild(0).GetComponent<TextMeshProUGUI>();
            var costText = child.GetChild(1).GetComponent<TextMeshProUGUI>();
            Obstacle obstacle = DestroyedList[i];

            int cost = obstacle.value*ScoreManager.Instance.obstacleToDestroyCount[obstacle];
            TotalCost += cost;

            icon.sprite = obstacle.icon;
            countText.text = "x " + ScoreManager.Instance.obstacleToDestroyCount[obstacle];
            costText.text = "= $" + cost;
        }

        var TotalText = ScorePanel.transform.GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>();
        TotalText.text = "$ " + TotalCost;

        NextButton.GetComponent<Button>().onClick.AddListener(()=>{
            MainController.Instance.LoadScene(SceneManager.GetActiveScene().name);
            CloseResultPanel();
        });
        NextButton.GetComponent<Button>().interactable = true;

        GenerateNewsInfo();
        CheckRate();
    }

    void CheckRate()
    {
        ScorePanel.transform.GetChild(1).GetComponent<Image>().enabled = true;
        if(isFailed)
            ScorePanel.transform.GetChild(1).GetComponent<Image>().sprite = RatingIcon[5];
        else if(TotalCost <= RateS)
            ScorePanel.transform.GetChild(1).GetComponent<Image>().sprite = RatingIcon[0];
        else if(TotalCost > RateS && TotalCost <= RateA)
            ScorePanel.transform.GetChild(1).GetComponent<Image>().sprite = RatingIcon[1];
        else if(TotalCost > RateA && TotalCost <= RateB)
            ScorePanel.transform.GetChild(1).GetComponent<Image>().sprite = RatingIcon[2];
        else if(TotalCost > RateB && TotalCost <= RateC)
            ScorePanel.transform.GetChild(1).GetComponent<Image>().sprite = RatingIcon[3];
        else if(TotalCost > RateC)
            ScorePanel.transform.GetChild(1).GetComponent<Image>().sprite = RatingIcon[4];
    }

    [Button]
    void GenerateNewsInfo()
    {
        var NewsText = NewsPanel.transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>();
        var NewsSubText = NewsPanel.transform.GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>();

        NewsText.text = NewsList.NewsList[Random.Range(0, NewsList.NewsList.Count)].Content;
        NewsSubText.text = NewsSubList.NewsList[Random.Range(0, NewsSubList.NewsList.Count)].Content;
        NewsPanel.transform.GetChild(0).GetComponent<Image>().sprite = NewsPicture[Random.Range(0, NewsPicture.Count)];
    }

    void ClearResultPanel()
    {
        ScorePanel.transform.GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text = "";
        ScorePanel.transform.GetChild(1).GetComponent<Image>().sprite = null;
        ScorePanel.transform.GetChild(1).GetComponent<Image>().enabled = false;
        NextButton.GetComponent<Button>().onClick.RemoveAllListeners();
        ScorePanel.transform.GetChild(2).GetComponent<Button>().interactable = false;

        NewsPanel.transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = "";
        NewsPanel.transform.GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = "";
        NewsPanel.transform.GetChild(0).GetComponent<Image>().sprite = null;

        TotalCost = 0;
    }

    [Button]
    public void OpenResultPanel(bool failed)
    {
        MainController.Instance.Ingame.CloseCurrentCost();
        foreach(var o in ScoreManager.Instance.obstacleToDestroyCount.Keys)
        {
            if(o.value >0)
                DestroyedList.Add(o);
        }

        MainPanel.SetActive(true);
        RefreshResultPanel();
    }

    [Button]
    public void CloseResultPanel()
    {
        MainPanel.SetActive(false);
        ClearResultPanel();
        DestroyedList.Clear();
    }
   
}
