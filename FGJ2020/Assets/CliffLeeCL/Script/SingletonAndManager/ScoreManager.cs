﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CliffLeeCL
{
    public class ScoreManager : SingletonMono<ScoreManager>
    {
        public Dictionary<Obstacle, int> obstacleToDestroyCount = new Dictionary<Obstacle, int>();

        public void AddDestroyCount(Obstacle obstacle)
        {
            if (obstacleToDestroyCount.ContainsKey(obstacle))
                obstacleToDestroyCount[obstacle] += 1;
            else
                obstacleToDestroyCount.Add(obstacle, 1);

            //Debug.Log(obstacle.icon.ToString() + ": " + obstacle.value + ", Total: " + obstacleToDestroyCount[obstacle]);
        }

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        public override void Awake()
        {
            base.Awake();
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            obstacleToDestroyCount.Clear();
        }

        void Start()
        {
            EventManager.Instance.onGameOver += OnGameOver;
        }

        void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            EventManager.Instance.onGameOver -= OnGameOver;
        }

        public void OnGameOver()
        {
            //UpdateHighestRecord();
        }

        public void UpdateHighestRecord()
        {
        }

    }
}
